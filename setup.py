from setuptools import setup, find_packages

def readme():
    with open('README.md') as f:
        return f.read()

setup(
    name='ReaderBOTDA',
    version='1.3.2',
    packages=find_packages(),
    include_package_data=True,
    scripts=['readBOTDA.py'],
    url='',
    license='',
    author='Marco Brunero',
    author_email='marco.brunero@cohaerentia.com',
    description='Pacchetto per lettura e visualizzazione file di log sensore BOTDA',
    long_description=readme(),
    python_requires='>=3.8.0',
    install_requires=[
        "dacite",
        "plotly>=5.1.0",
        "setuptools~=58.5.3",
        "bokeh>=2.4.2",
        "typer>=0.4.0",
        "progress>=1.6",
    ],
)