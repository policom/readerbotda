1.3.2 - 2023/12/11 - Campi relativi a metodo polyfit in Settings.
1.3.1 - 2023/12/05 - Aggiunto sw_version, letto da json se presente
1.3.0 - 2023/10/25 - Funzione calcCorrelations per calcolo correlazione in multipleProfile
1.2.5 - 2023/10/19 - Aggiunti altri metodi per slicing di multipleProfile
1.2.4 - 2023/10/18 - Aggiunto parametro start_measure in multipleProfile
1.2.3 - 2023/10/18 - Aggiunto parametro n_measure in multipleProfile
1.2.2 - 2023/08/01 - Plot Max di multipleBFS. Dataclass Statistics per media, std di multipleBFS
1.2.1 - 2023/02/24 - Comando cli readBOTDA incluso nel pacchetto, in teoria
1.2.0 - 2023/02/22 - Aggiunta compatibilità con rawarray 2023, che includono array dei Max BGS
1.0.8 - 2021/12/06 - Iniziata CLI ma non so come fare packaging
1.0.0 - 2021/12/06 - Aggiunta versione plotter Bokeh
0.9.1 - 2021/11/30 - Rimosso separatore migliaia da ticks
0.9.0 - 2021/11/23 - Creazione pacchetto